import unittest
from sportpursuit_coding_challenge.main import Bike


class TestBike(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_simple_bike(self):
        self.bike = Bike(1, ['20150304T00:00:00'], ['20150304T01:00:00'])

        self.assertEquals(self.bike.avg_duration)

if __name__ == '__main__':
    unittest.main()
