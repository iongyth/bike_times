import csv

from sportpursuit_coding_challenge.Bike import Bike


def load_data():
    """
    Retrieves the data from the csv file and loads the data inside the relevant
    object.

    :return: dict containing Bike objects
    """
    bikes = dict()

    with open('data.csv', 'r') as f:
        data = csv.reader(f, delimiter=',')
        for row in data:
            _, bike_no, arr, dep = row

            if bike_no not in bikes.keys():
                bikes[bike_no] = Bike(bike_no)

            this_bike = bikes[bike_no]
            this_bike.add_arr_time(arr)
            this_bike.add_dep_time(dep)

    return bikes

if __name__ == '__main__':
    dock = load_data()

    for no, bike in dock.items():
        print(bike)


