from datetime import timedelta, datetime


class Bike:
    def __init__(self, bike_no, arr_times=None, dep_times=None):
        self.bike_no = bike_no
        self.t_arrival = arr_times if arr_times else []
        self.t_depart = dep_times if dep_times else []
        self.journey_duration = timedelta(seconds=0)

    def __repr__(self):
        return f"<{self.bike_no}>: {self.t_arrival} {self.t_depart} {self.avg_duration}"

    @property
    def is_used(self):
        return bool(self.t_arrival and self.t_depart)

    def add_arr_time(self, arr_time):
        if arr_time:
            self.t_arrival.append(arr_time)

    def add_dep_time(self, dep_time):
        if dep_time:
            self.t_depart.append(dep_time)

    def _pair_times(self):
        self.t_arrival = sorted(self.t_arrival)
        self.t_depart = sorted(self.t_depart)

        if not self.is_used:
            return

        # remove first arrival and/or last departure if the bike was being used
        # when the reporting period began/finished
        if self.t_arrival[0] < self.t_depart[0]:
            del self.t_arrival[0]

        if self.t_depart[-1] > self.t_arrival[-1]:
            del self.t_depart[-1]

    @property
    def time_used(self):
        self._pair_times()

        self.journey_duration = timedelta(seconds=0)
        for arr, dep in zip(self.t_arrival, self.t_depart):
            arr = datetime.strptime(arr, '%Y%m%dT%H:%M:%S')
            dep = datetime.strptime(dep, '%Y%m%dT%H:%M:%S')
            self.journey_duration += (arr - dep)

        return self.journey_duration

    @property
    def avg_duration(self):
        self._pair_times()

        if not self.is_used:
            return timedelta(seconds=0)

        self.journey_duration = timedelta(seconds=0)
        for arr, dep in zip(self.t_arrival, self.t_depart):
            arr = datetime.strptime(arr, '%Y%m%dT%H:%M:%S')
            dep = datetime.strptime(dep, '%Y%m%dT%H:%M:%S')
            self.journey_duration += (arr - dep)

        t_average = self.journey_duration / len(self.t_arrival)

        hours, remainder = divmod(t_average.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)

        return '%02d:%02d:%02d' % (hours, minutes, seconds)
